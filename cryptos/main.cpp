#include <iostream>
#include <fstream>
#include "GOST_28147_89.h"


void convertKey(const char * keyStr, unsigned char *encodedKey) {
	for(int count = 0; count < 32; count++) {
		sscanf(keyStr, "%2hhx", &encodedKey[count]);
		keyStr += 2;
	}
}

int main(int argc, const char * argv[]) {
	const char * inputFile, *outputFile;
	const char *rawKey;
	bool isEncrypt;
	for (int i = 0; i < argc; i++) {
		const char *current = argv[i];
		if (!strcmp(current, "-encrypt")) {
			isEncrypt = true;
			rawKey = argv[i + 1];
			i++;
			continue;
		}
		if (!strcmp(current, "-decrypt")) {
			isEncrypt = false;
			rawKey = argv[i + 1];
			i++;
			continue;
		}
		if (!strcmp(current, "-input")) {
			inputFile = argv[i + 1];
			i++;
			continue;
		}
		if (!strcmp(current, "-output")) {
			outputFile = argv[i + 1];
			i++;
			continue;
		}
	}

	if (!rawKey) {
		std::cout << "No method provided, terminate...\n";
		return -1;
	}

	unsigned char key[32];
	convertKey(rawKey, key);
	GOST_28147_89 *algo = new GOST_28147_89(key);
	std::istream *input;
	std:: ostream *output;
	if (inputFile) {
		input = new std::ifstream(inputFile);
	} else {
		input = &std::cin;
	}
	if (outputFile) {
		output = new std::ofstream(outputFile);
	} else {
		output = &std::cout;
	}

	if (isEncrypt) {
		algo->encrypt(*input, *output);
	} else {
		algo->decrypt(*input, *output);
	}

	if (inputFile) {
		dynamic_cast<std::ifstream *>(input)->close();
	}
	if (outputFile) {
		dynamic_cast<std::ofstream *>(output)->close();
	}
    return 0;
}
