//
// Created by Eugene Butusov on 15/12/15.
// Copyright (c) 2015 vedidev. All rights reserved.
//

#include <stack>
#include <queue>
#include "GOST_28147_89.h"
#include "DataConversion.h"

// substitution blocks, given from https://tools.ietf.org/html/rfc4357#section-11.2 (used by ЦБ РФ for digital signatures)
const unsigned char S[8][16] = {
        { 4, 10, 9, 2, 13, 8, 0, 14, 6, 11, 1, 12, 7, 15, 5, 3 },
        { 14, 11, 4, 12, 6, 13, 15, 10, 2, 3, 8, 1, 0, 7, 5, 9 },
        { 5, 8, 1, 13, 10, 3, 4, 2, 14, 15, 12, 7, 6, 0, 9, 11 },
        { 7, 13, 10, 1, 0, 8, 9, 15, 14, 4, 6, 12, 11, 2, 5, 3 },
        { 6, 12, 7, 1, 5, 15, 13, 8, 4, 10, 9, 14, 0, 3, 11, 2 },
        { 4, 11, 10, 0, 7, 2, 1, 13, 3, 6, 8, 5, 9, 12, 15, 14 },
        { 13, 11, 4, 1, 3, 15, 5, 9, 0, 10, 14, 7, 6, 8, 2, 12 },
        { 1, 15, 13, 0, 5, 7, 10, 4, 9, 2, 3, 14, 6, 11, 8, 12 }
};

// constructor, convert unsigned char sequence to sequence of 32-bit integer values
GOST_28147_89::GOST_28147_89(const unsigned char * key) {
    this->keys = prepareKeys(key);
}

// convert unsigned char sequence to sequence of 32-bit integer values
uint32_t *GOST_28147_89::prepareKeys(const unsigned char *key) {
    uint32_t *result = new uint32_t[8];
    for (int i = 0; i < 8; i++) {
        result[i] = DataConversion::convert(key + i * 4);
    }
    return result;
}

// perform substitution from S-block
uint32_t GOST_28147_89::substitute(uint32_t value) {
    std::stack<unsigned char> bearer;
    uint32_t result = 0;
    for (int i = 0; i < 8; i++) {
        bearer.push(S[8 - (i + 1)][value % 16]);
        value >>= 4;
    }
    while (!bearer.empty()) {
        result = (result << 4) + bearer.top();
        bearer.pop();
    }
    return result;
}

// evaluate F-function for the first part of data block
uint32_t GOST_28147_89::evaluate(uint32_t value, uint32_t key) {
    value = this->substitute(value + key);
    return ((value << 11) | (value >> 21));

}

// as decrypt is the same as encrypt, but with reverted order of keys, we can make it with one method
void GOST_28147_89::applyKeys(std::istream &input, std::ostream &output, bool keyOrder) {
    unsigned char *buffer = new unsigned char[8];
    // while not EOF or something bad with input stream
    do {
        // read values, use reinterpret cast, because we can read only chars
        std::streamsize extracted = input.read(reinterpret_cast<char *>(buffer), 8).gcount();
        if (extracted == 0) {
            continue;
        }

        // if buffer has less than 8 bytes (32-bit block), fill the extra data with zero-vals
        for (int i = (int)extracted; i < 8; i++) {
            buffer[i] = 0;
        }

        // as before, convert unsigned chars to unsigned 32-bit integers
        uint32_t *convertedBuffer = new uint32_t[2];
        convertedBuffer[0] = DataConversion::convert(buffer);
        convertedBuffer[1] = DataConversion::convert(buffer + 4);

        // 32-round encryption cycle
        for (int i = keyOrder ? 0 : 31; keyOrder ? (i < 32) : (i >= 0); keyOrder ? i++ : i--) {
            uint32_t key = (i < 24) ? keys[i % 8] : keys[31 - i];
            uint32_t evaluatedBlock = convertedBuffer[1] ^ evaluate(convertedBuffer[0], key);

            // extra step for the last iteration
            if((keyOrder && i == 31) || (!keyOrder && i == 0)) {
                convertedBuffer[1] = evaluatedBlock;
            } else {
                convertedBuffer[1] = convertedBuffer[0];
                convertedBuffer[0] = evaluatedBlock;
            }
        }

        unsigned char * outputBuffer = new unsigned char[8];
        for (int i = 0; i < 2; i++) {
            unsigned char *tmpBuffer = DataConversion::convert(convertedBuffer[i]);
            for (int j = 0; j < 4; j++) {
                outputBuffer[i * 4 + j] = tmpBuffer[j];
            }
        }
        output.write(reinterpret_cast<char *>(outputBuffer), 8);
    } while (!input.eof());
}

void GOST_28147_89::encrypt(std::istream &input, std::ostream &output) {
    this->applyKeys(input, output, true);
}

void GOST_28147_89::decrypt(std::istream &input, std::ostream &output) {
    this->applyKeys(input, output, false);
}