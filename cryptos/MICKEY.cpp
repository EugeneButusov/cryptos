//
// Created by Eugene Butusov on 18/01/16.
// Copyright (c) 2016 vedidev. All rights reserved.
//

#include "MICKEY.h"

//inputs of R register
unsigned char RTAPS[50] = { 0, 1, 3, 4, 5, 6, 9, 12, 13, 16, 19, 20, 21, 22, 25, 28, 37, 38, 41, 42, 45, 46, 50, 52,
        54, 56, 58, 60, 61, 63, 64, 65, 66, 67, 71, 72, 79, 80, 81, 82, 87, 88, 89, 90, 91, 92, 94, 95, 96, 97 };

//values used in S register clocking
unsigned char COMP0[100] = {
        0,0,0,0,1,1,0,0,0,1,0,1,1,1,1,0,1,0,0,1,0,1,0,1,0,
        1,0,1,0,1,1,0,1,0,0,1,0,0,0,0,0,0,0,1,0,1,0,1,0,1,
        0,0,0,0,1,0,1,0,0,1,1,1,1,0,0,1,0,1,0,1,1,1,1,1,1,
        1,1,1,0,1,0,1,1,1,1,1,1,0,1,0,1,0,0,0,0,0,0,1,1,0
};

//values used in S register clocking
unsigned char COMP1[100] = {
        0,1,0,1,1,0,0,1,0,1,1,1,1,0,0,1,0,1,0,0,0,1,1,0,1,
        0,1,1,1,0,1,1,1,1,0,0,0,1,1,0,1,0,1,1,1,0,0,0,0,1,
        0,0,0,1,0,1,1,1,0,0,0,1,1,1,1,1,1,0,1,0,1,1,1,0,1,
        1,1,1,0,0,0,1,0,0,0,0,1,1,1,0,0,0,1,0,0,1,1,0,0,0
};

//values used in S register clocking
unsigned char FB0[100] = {
        1,1,1,1,0,1,0,1,1,1,1,1,1,1,1,0,0,1,0,1,1,1,1,1,1,
        1,1,1,1,0,0,1,1,0,0,0,0,0,0,1,1,1,0,0,1,0,0,1,0,1,
        0,1,0,0,1,0,1,1,1,1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,
        1,1,0,1,0,0,0,1,1,0,1,1,1,0,0,1,1,1,0,0,1,1,0,0,0
};

//values used in S register clocking
unsigned char FB1[100] = {
        1,1,1,0,1,1,1,0,0,0,0,1,1,1,0,1,0,0,1,1,0,0,0,1,0,
        0,1,1,0,0,1,0,1,1,0,0,0,1,1,0,0,0,0,0,1,1,0,1,1,0,
        0,0,1,0,0,0,1,0,0,1,0,0,1,0,1,1,0,1,0,1,0,0,1,0,1,
        0,0,0,1,1,1,1,0,1,1,1,1,1,0,0,0,0,0,0,1,0,0,0,0,1
};

//check if value is in inputs of R register
bool isInRTAPS(int val) {
    for (int i = 0; i < 50; i++) {
        if (val == RTAPS[i]) {
            return true;
        }
    }
    return false;
}

//clock for R register
void MICKEY::CLOCK_R(unsigned char *R, unsigned char input_bit, unsigned char control_bit) {
    unsigned char feedback_bit = R[99] ^ input_bit;
    unsigned char newR[100];

    for (int i = 1; i < 100; i++) {
        newR[i] = R[i - 1];
    }
    newR[0] = 0;

    for (int i = 0; i < 100; i++) {
        if (isInRTAPS(i)) {
            newR[i] = newR[i] ^ feedback_bit;
        }
    }

    if (control_bit) {
        for (int i = 0; i < 100; i++) {
            newR[i] = newR[i] ^ R[i];
        }
    }

    for (int i = 0; i < 100; i++) {
        R[i] = newR[i];
    }
}

//clock for S register
void MICKEY::CLOCK_S(unsigned char *S, unsigned char input_bit, unsigned char control_bit) {
    unsigned char newS[100], intermediateS[100];

    unsigned char feedback_bit = S[99] ^ input_bit;

    for (int i = 1; i < 99; i++) {
        intermediateS[i] = S[i - 1] ^ ((S[i] ^ COMP0[i]) & (S[i + 1] ^ COMP1[i]));
    }
    intermediateS[0] = 0;
    intermediateS[99] = S[98];

    for (int i = 0; i < 100; i++) {
        if (control_bit) {
            newS[i] = intermediateS[i] ^ (FB1[i] & feedback_bit);
        } else {
            newS[i] = intermediateS[i] ^ (FB0[i] & feedback_bit);
        }
    }

    for (int i = 0; i < 100; i++) {
        S[i] = newS[i];
    }
}

//clock for whole keystream generator
void MICKEY::CLOCK_KG(unsigned char *R, unsigned char *S, bool mixing, unsigned char input_bit) {
    unsigned char control_bit_r = S[34] ^ R[67],
                  control_bit_s = S[67] ^ R[33];

    unsigned char input_bit_r = mixing ? (input_bit ^ S[50]) : input_bit,
                  input_bit_s = input_bit;

    this->CLOCK_R(R, input_bit_r, control_bit_r);
    this->CLOCK_S(S, input_bit_s, control_bit_s);
}

MICKEY::MICKEY(const unsigned char *key, const unsigned char *iv, const unsigned char iv_length) {

    //memset for registers
    for (int i = 0; i < 100; i++) {
        R[i] = 0;
        S[i] = 0;
    }

    //initialization with IV (initial variable)
    for (int i = 0; i < iv_length; i++) {
        unsigned char value = iv[i];
        for (int j = 0; j < sizeof(value) * 8; j++) {
            this->CLOCK_KG(R, S, true, (unsigned char)(value % 2));
            value >>= 1;
        }
    }

    //initialization with key
    for (int i = 0; i < 10; i++) {
        unsigned char value = key[i];
        for (int j = 0; j < sizeof(value) * 8; j++) {
            this->CLOCK_KG(R, S, true, (unsigned char)(value % 2));
            value >>= 1;
        }
    }

    //preloop
    for (int i = 0; i < 100; i++) {
        this->CLOCK_KG(R, S, true, (unsigned char)0);
    }
}

//emit one byte of keystream
unsigned char MICKEY::EMIT_GAMMA() {
    unsigned char result = 0;
    for (int i = 0; i < sizeof(result) * 8; i++) {
        result <<= 1;
        result += (R[0] ^ S[0]);
        this->CLOCK_KG(R, S, false, (unsigned char)0);
    }
    return result;
}

//combine input stream with keystream and put it into output
void MICKEY::PROCESS_STREAM(std::istream &input, std::ostream &output) {
    unsigned char input_buffer;

    while (!input.eof()) {
        if (!input.read(reinterpret_cast<char *>(&input_buffer), 1)) {
            break;
        }

        output << (unsigned char)(input_buffer ^ this->EMIT_GAMMA());
    }
}

//encrypt and decrypt is the same but kept to respond parent class interface
void MICKEY::encrypt(std::istream &input, std::ostream &output) {
    this->PROCESS_STREAM(input, output);
}

//encrypt and decrypt is the same but kept to respond parent class interface
void MICKEY::decrypt(std::istream &input, std::ostream &output) {
    this->PROCESS_STREAM(input, output);
}