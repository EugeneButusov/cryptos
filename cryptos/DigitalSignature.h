//
// Created by Eugene Butusov on 19/01/16.
// Copyright (c) 2016 vedidev. All rights reserved.
//

#ifndef CRYPTOS_DIGITALSIGNATURE_H
#define CRYPTOS_DIGITALSIGNATURE_H

#include <string>

class Signature {

};

class DigitalSignature {
    public:
        Signature sign(std::string message);
        bool validate(std::string message, Signature signature);
};


#endif //CRYPTOS_DIGITALSIGNATURE_H
