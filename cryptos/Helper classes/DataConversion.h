//
// Created by Eugene Butusov on 15/12/15.
// Copyright (c) 2015 vedidev. All rights reserved.
//

#ifndef CRYPTOS_DATACONVERSION_H
#define CRYPTOS_DATACONVERSION_H

#include <stdint.h>

class DataConversion {
    public:
        static uint32_t convert(const unsigned char *values);
        static unsigned char *convert(uint32_t value);
};


#endif //CRYPTOS_DATACONVERSION_H
