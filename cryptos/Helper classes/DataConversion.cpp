//
// Created by Eugene Butusov on 15/12/15.
// Copyright (c) 2015 vedidev. All rights reserved.
//

#include <stdio.h>
#include "DataConversion.h"

uint32_t DataConversion::convert(const unsigned char *values) {
    uint32_t result = 0;
    for (int i = 0; i < 4; i++) {
        result = (result << 8) + values[i];
    }
    return result;
}

unsigned char *DataConversion::convert(uint32_t value) {
    unsigned char *result = new unsigned char[4];
    for (int i = 0; i < 4; i++) {
        result[4 - (i + 1)] = (unsigned char)(value % 256);
        value >>= 8;
    }
    return result;
}