//
// Created by Eugene Butusov on 15/12/15.
// Copyright (c) 2015 vedidev. All rights reserved.
//

#ifndef CRYPTOS_CRYPTOMETHOD_H
#define CRYPTOS_CRYPTOMETHOD_H

#include <iostream>

//parent class working with streams to encrypt/decrypt
class CryptoMethod {
    public:
        virtual void encrypt(std::istream &input, std::ostream &output) = 0;
        virtual void decrypt(std::istream &input, std::ostream &output) = 0;
};


#endif //CRYPTOS_CRYPTOMETHOD_H
