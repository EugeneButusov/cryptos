//
// Created by Eugene Butusov on 15/12/15.
// Copyright (c) 2015 vedidev. All rights reserved.
//

#ifndef CRYPTOS_GOST_28147_89_H
#define CRYPTOS_GOST_28147_89_H


#include "CryptoMethod.h"

class GOST_28147_89 : public CryptoMethod {
    private:
        uint32_t * keys;
        uint32_t * prepareKeys(const unsigned char * key);

        void applyKeys(std::istream &input, std::ostream &output, bool keyOrder);

        uint32_t substitute(uint32_t value);
        uint32_t evaluate(uint32_t value, uint32_t key);
    public:
        GOST_28147_89(const unsigned char * key);
        virtual void encrypt(std::istream &input, std::ostream &output);
        virtual void decrypt(std::istream &input, std::ostream &output);
};


#endif //CRYPTOS_GOST_28147_89_H
