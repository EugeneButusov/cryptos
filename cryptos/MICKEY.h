//
// Created by Eugene Butusov on 18/01/16.
// Copyright (c) 2016 vedidev. All rights reserved.
//

#ifndef CRYPTOS_MICKEY_H
#define CRYPTOS_MICKEY_H


#include "CryptoMethod.h"

class MICKEY : public CryptoMethod {
    private:
        //registers
        unsigned char R[100];
        unsigned char S[100];

        void CLOCK_R(unsigned char * R, unsigned char input_bit, unsigned char control_bit);
        void CLOCK_S(unsigned char * S, unsigned char input_bit, unsigned char control_bit);
        void CLOCK_KG(unsigned char * R, unsigned char * S, bool mixing, unsigned char input_bit);

        unsigned char EMIT_GAMMA();

        void PROCESS_STREAM(std::istream &input, std::ostream &output);


    public:
        MICKEY(const unsigned char * key, const unsigned char * iv, const unsigned char iv_length);
        virtual void encrypt(std::istream &input, std::ostream &output);
        virtual void decrypt(std::istream &input, std::ostream &output);
};


#endif //CRYPTOS_MICKEY_H
